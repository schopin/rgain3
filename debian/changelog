rgain3 (1.1.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Simon McVittie ]
  * New upstream release
  * d/control: Version some dependencies
  * d/copyright: Update
  * Drop patches that were applied upstream
  * Standards-Version: 4.5.1 (no changes required)
  * Use pytest to run build-time tests
  * d/control: Add a warning about API stability
  * Mark autopkgtest as flaky.
    This seems to be reliable on a real machine but sometimes fails in an
    autopkgtest VM. See https://github.com/chaudum/rgain3/issues/37
  * d/gbp.conf: Set packaging branch to debian/latest

 -- Simon McVittie <smcv@debian.org>  Mon, 09 Aug 2021 13:21:57 +0100

rgain3 (1.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Simon McVittie ]
  * d/salsa-ci.yml: Request standard CI on salsa.debian.org
  * Switch to a new upstream source, supporting Python 3
    (Closes: #960310) (reopens: #798137, #829053, #893660)
    - The upstream version number has been reset to 1.0.0 with the switch to
      a new upstream name, but this is harmless because the source package
      and all binary packages were renamed. The next release of rgain3 will
      probably be versioned 3.x to be less confusing.
    - Update dependencies
    - Update name of README
    - d/copyright: Update
    - Include binary test data.
      This was not included in the upstream tarball release (which will
      hopefully be fixed in the next release), but is necessary to run the
      tests. It consists of several artificial 1-second audio files.
  * Separate replaygain(1) and collectiongain(1) into their own binary
    package to make them more discoverable (Closes: #780915)
    - Add Breaks/Replaces on the removed package python-rgain.
      They cannot be co-installed because both provide replaygain(1)
      and collectiongain(1).
  * d/p/setup.cfg-Only-run-functional-checks-during-Debian-packag.patch:
    Disable non-functional checks (coding style, etc.) during package
    build. These checks add extra dependencies, increase fragility
    (failing when a subjective coding style check is changed), and are
    not really useful for our purposes.
  * d/tests/python3: Run tests as an autopkgtest
  * d/tests/replaygain: Add a test for the replaygain(1) CLI
    - d/tests/*.oga: Copy some Free Software audio files from
      sound-theme-freedesktop to have non-trivial data to test against.
      They are included here to avoid the tests spuriously failing if
      the sounds in sound-theme-freedesktop are changed.
  * d/control, d/p/Replace-link-to-replaygain.org.patch:
    Replace dead links.
    replaygain.org doesn't seem to be under the control of the author of
    the ReplayGain specification any more, and the Hydrogenaudio wiki
    appears to be the de facto reference now.
  * d/p/Use-GLib.MainLoop-instead-of-GObject.MainLoop.patch,
    d/p/Replace-deprecated-assertEquals-with-assertEqual.patch:
    Apply upstreamed patches to fix deprecation warnings
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Move to debhelper compat level 13
  * d/p/0001-build-Use-a-reproducible-timestamp-for-man-pages.patch:
    Drop patch, no longer necessary.
    The man pages no longer appear to include a date.

 -- Simon McVittie <smcv@debian.org>  Sun, 24 May 2020 12:43:33 +0100

rgain (1.3.4-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

  [ Simon McVittie ]
  * Standards-Version: 4.3.0 (no changes required)
  * Set Rules-Requires-Root to no
  * Normalize dependency lists (wrap-and-sort -abst)
  * Use debhelper-compat 12 and pybuild

 -- Simon McVittie <smcv@debian.org>  Sun, 03 Feb 2019 15:34:21 +0000

rgain (1.3.4-3) unstable; urgency=medium

  * Standards-Version: 4.1.4 (no changes required)
  * Set Testsuite: autopkgtest-pkg-python to enable a standard autodep8
    smoke-test
  * Switch packaging git repository from git dpm to a patches-unapplied
    layout suitable for gbp pq, with DEP-14 branch names

 -- Simon McVittie <smcv@debian.org>  Fri, 20 Apr 2018 10:23:28 +0100

rgain (1.3.4-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Simon McVittie ]
  * Remove Simon Chopin from Uploaders, no longer active in Debian.
    Thanks for your work on this package!
  * Add myself to Uploaders (Closes: #889778)
  * d/p/0001-build-Use-a-reproducible-timestamp-for-man-pages.patch:
    Make the date in the man pages reproducible across changes to date
    and local time zone
  * Build-depend on dh-python
  * Standards-Version: 4.1.3 (no changes required)
  * Advance to debhelper compat level 11

 -- Simon McVittie <smcv@debian.org>  Wed, 14 Mar 2018 09:12:11 +0000

rgain (1.3.4-1) unstable; urgency=medium

  * Team upload.

  [ Debian Python Modules Team ]
  * Move from svn to git-dpm

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Simon McVittie ]
  * Update debian/watch file to use pypi.debian.net
  * New upstream version
    - d/copyright: update
  * Recommend gstreamer1.0-plugins-ugly | gstreamer1.0-libav, for
    MP3 decoding via libmad or ffmpeg respectively (Closes: #822859)
  * Standards-Version: 3.9.8 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Tue, 26 Jul 2016 22:49:11 +0100

rgain (1.3.3-1) unstable; urgency=medium

  * New upstream release
  * Bump copyright years
  * Bump Standards version to 3.9.6, no changes needed.
  * Bump supported Python version to 2.6, as it is supported upstream

 -- Simon Chopin <chopin.simon@gmail.com>  Thu, 16 Oct 2014 05:39:25 +0200

rgain (1.3.2-1) unstable; urgency=low

  * Team upload.

  [ Simon Chopin ]
  * New upstream release (Closes: #754286)
  * Add gstreamer1.0-plugins-bad to Recommends.
    This allows the use of replaygain on MP3s (Closes: #742508)
  * Bump Standards version to 3.9.5, no changes needed.

 -- Simon McVittie <smcv@debian.org>  Fri, 12 Sep 2014 09:14:13 +0100

rgain (1.2-1) unstable; urgency=low

  [ Simon Chopin ]
  * New upstream release.
  * Now Depends on python-gi and gstreamer 1.0
  * Bump Standards version to 3.9.4:
    + B-D on debhelper >= 8.1 to get build-arch and build-indep targets
  * Update copyright years and use the Copyright-Format 1.0 documentation.
  * Use the upstream man pages

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 14 May 2013 21:53:51 +0200

rgain (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #650154)

 -- Simon Chopin <chopin.simon@gmail.com>  Thu, 01 Dec 2011 00:53:18 +0200
